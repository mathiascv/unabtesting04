import math as m
import random as r
class Calculator:
    
    def add(x, y):
        return x + y

    def subtract(x, y):
        return x - y
     
    def multiply(x, y):
        return x * y
     
    def divide(x, y):
    	if y == 0:
    		print("\nError: Division por 0")
    		return 0
    	else:
        	return x / y
#######Agregadas por mi##########
#################################
    def potencia(x, y):
    	result = 1
    	if y < 0:
    		print("Error: Calculadora no soporta exponente negativo.")
    		return 0
    	else:
	    	for i in range(0,y):
	    		result*=x
	    	return result

    def calculoRandom(x, y):
    	num = r.choice([1,2,3,4,5])
    	if num == 1:
    		print("\nSuma: ")
    		return x + y
    	elif num == 2:
    		print("\nResta: ")
    		return x - y
    	elif num == 3:
    		print("\nMultiplicacion: ")
    		return x * y
    	elif num == 4 and y!=0:
    		print("\nDivicion: ")
    		return x / y
    	elif num==5:
    		print("\nPromedio")
    		return (x+y)/2
    	else:
    		print("\nError: Divicion por 0: ")
    		return -1

    def promedio(x, y):
    	prom = (y+x)/2
    	return prom

