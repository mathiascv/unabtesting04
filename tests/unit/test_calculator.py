import unittest
from app.calculator import Calculator

class TestCalculatorAdd(unittest.TestCase):
        def test_add_positive(self):
            result = Calculator.add(1,100)
            self.assertEqual(result, 101)
        def test_add_negative(self):
            result = Calculator.add(-1,-100)
            self.assertEqual(result, -101)
        def test_add_string(self):
            result = Calculator.add(1,-102)
            self.assertEqual(result, -101)
            
class TestCalculatorDivide(unittest.TestCase):
        def test_divide_positive(self):
            result = Calculator.divide(4,2)
            self.assertEqual(result, 2)
        def test_divide_byzero(self):
            result = Calculator.divide(4,0)
            self.assertIsNone(result, 0)
        def test_division_neg(unittest):
            result = Calculator.divide(-1,-1)
            self.assertEqual(result, 1)

class TestCalculatorMultiply(unittest.TestCase):
        def test_multiplicacion_byzero(self):
            result = Calculator.multiply(1,0):
            self.assertEqual(result,0)
        def test_multiplicacion_neg(self):
            result = Calculator.multiply(-1,100)
            self.assertEqual(result, -100)

class TestCalculatorPotencia(unittest.TestCase):
        def test_potencia_cero(self):
            result = Calculator.potencia(10,0)
            self.assertEqual(result, 1)
        def test_potencia_exponente_neg(self):
            result = Calculator.potencia(10, -2):
            self.assertEqual(result, 0)

class TestCalculatorPromedio(unittest.TestCase):
        def test_promedio_negativo(self):
            result = Calculator.promedio(-1,-1)
            self.assertEqual(result, -1)
        
if __name__ == '__main__':
    unittest.main()
